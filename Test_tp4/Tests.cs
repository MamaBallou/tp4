﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TP4;

namespace Test_tp4
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void Test_carre()
        {
            Assert.AreEqual(Program.CalculAire_carre(2), 4);
            Assert.AreEqual(Program.CalculAire_carre(-3), -1);
            Assert.AreEqual(Program.CalculAire_carre(0), 0);

        }

        [TestMethod]
        public void Test_rectangle()
        {
            Assert.AreEqual(Program.CalculAire_rectangle(2, 3), 6);
            Assert.AreEqual(Program.CalculAire_rectangle(-1, 2.3), -1);
        }

        [TestMethod]
        public void Test_triangle()
        {
            Assert.AreEqual(Program.CalculAire_triangle(2, 2), 2);
            Assert.AreEqual(Program.CalculAire_triangle(2, -1), -1);
        }

        [TestMethod]
        public void Test_Majorite()
        {
            Assert.IsTrue(Program.estMajeur(DateTime.Now.Year - 18));
            Assert.IsFalse(Program.estMajeur(DateTime.Now.Year - 15));
        }

        [TestMethod]
        public void Test_int_dans_tableau()
        {
            int[] tableau = new int[2];
            tableau[0] = 1;
            tableau[1] = 1;
            Assert.IsFalse(Program.int_appartientAuTableau(tableau, 3));
            Assert.IsTrue(Program.int_appartientAuTableau(tableau, 1));
        }

        [TestMethod]
        public void Test_string_dans_tableau()
        {
            string[] tableau = new string[2];
            tableau[0] = "Hello World";
            tableau[1] = "Goodbye World";
            Assert.IsFalse(Program.string_appartientAuTableau(tableau, "HelloWorld"));
            Assert.IsTrue(Program.string_appartientAuTableau(tableau, "Goodbye World"));
        }

        [TestMethod]
        public void Test_int_positionDansTableau()
        {
            int[] tableau = new int[2];
            tableau[0] = 1;
            tableau[1] = 2;
            Assert.AreEqual(Program.int_position_dans_tableau(tableau, 1, 2), 0);
            Assert.AreEqual(Program.int_position_dans_tableau(tableau, 3, 2), -1);
        }

        [TestMethod]
        public void Test_string_positionDansTableau()
        {
            string[] tableau = new string[2];
            tableau[0] = "Hello World";
            tableau[1] = "Goodbye World";
            Assert.AreEqual(Program.string_position_dans_tableau(tableau, "HelloWorld", 2), -1);
            Assert.AreEqual(Program.string_position_dans_tableau(tableau, "Goodbye World", 2), 1);
        }
    }
}
