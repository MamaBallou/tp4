﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP4
{
    public class Program
    {
        public static double CalculAire_carre(double cote)
        {
            if (cote < 0) return -1.0;
            return cote * cote;
        }

        public static double CalculAire_rectangle(double longueur, double largeur)
        {
            if ((longueur < 0) || (largeur < 0)) return -1.0;
            return longueur * largeur;
        }

        public static double CalculAire_triangle(double Base, double hauteur)
        {
            if ((Base < 0) || (hauteur < 0)) return -1;
            return (Base * hauteur) / 2;
        }

        public static bool estMajeur(int annee_nais)
        {
            if (DateTime.Now.Year - annee_nais < 18) return false;
            return true;
        }

        public static bool int_appartientAuTableau(int[] tableau, int valeur_cherchee)
        {
            foreach (int Valeur in tableau)
            {
                if (Valeur == valeur_cherchee) return true;
            }
            return false;
        }

        public static bool string_appartientAuTableau(string[] tableau, string phrase_cherchee)
        {
            foreach (string phrase in tableau)
            {
                if (phrase == phrase_cherchee) return true;
            }
            return false;
        }

        public static int int_position_dans_tableau(int[] tableau, int valeur_cherchee, int taille_tableau)
        {
            for (int compteur = 0; compteur < taille_tableau; compteur++)
            {
                if (tableau[compteur] == valeur_cherchee) return compteur;
            }
            return -1;
        }

        public static int string_position_dans_tableau(string[] tableau, string phrase_cherchee, int taille_tableau)
        {
            for (int compteur = 0; compteur < taille_tableau; compteur++)
            {
                if (tableau[compteur] == phrase_cherchee) return compteur;
            }
            return -1;
        }

        static void Main(string[] args)
        {
        }
    }
}
